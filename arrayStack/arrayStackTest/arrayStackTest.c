#include <stdio.h>
#include <stdlib.h>
#include "/mnt/files/CUnit/include/CUnit/CUnit.h"
#include "/mnt/files/dataStructures/arrayStack/src/arrayStack.h"

//Data struct
typedef struct Data_s
{
    size_t id;    
} Data_s;

void deleteData(void* pData)
{
    //cast void* to implemenation specific type
    Data_s* pThisData = (Data_s*) pData;

    //free malloced memory        
    free(pThisData);

}

void printData(const void* pData)
{
    //cast void* to implimenation specific type
    Data_s* pThisData = (Data_s*) pData;

    printf("%zu: \n", pThisData->id);    
}

int main()
{
    ArrayStack_s* pArrayStack = newArrayStack(7);
    
    size_t i = 0;
    
    for (i = 0; i < 50; i++)
    {
        Data_s* pData = malloc(sizeof(Data_s));
        pData->id = i;
        pushStack(pArrayStack, pData);        
    }
        
    for (i = 0; i < 50; i++)
    {
        Data_s* pThisData = (Data_s*) popStack(pArrayStack);
        printData(pThisData);
        deleteData(pThisData);
    }
    destroy(pArrayStack, deleteData);
    
    return 0;
}