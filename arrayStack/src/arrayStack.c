#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "arrayStack.h"

typedef struct ArrayStack_s
{
    size_t uiSize;              //size of the backing array    
    size_t uiCurrentPosition;   //Value keeps track of the tail end of queue
    void** backingArray;        //array containing pointers to the data
} ArrayStack_s;

void initArrayStack(ArrayStack_s* pArrayStack, size_t uiInitialSize)
{
    if (uiInitialSize%2 != 0)
    {   
        //if initial size not even, make even
        uiInitialSize = uiInitialSize + 1;
    }
    pArrayStack->uiSize = uiInitialSize;
    pArrayStack->uiCurrentPosition = 0;    
    pArrayStack->backingArray = malloc(uiInitialSize * sizeof(void*)); 
}

ArrayStack_s* newArrayStack(size_t uiInitialSize)
{
    ArrayStack_s* pArrayStack = malloc(sizeof(ArrayStack_s));
    initArrayStack(pArrayStack, uiInitialSize);
    
    return pArrayStack;
}

void resizeBackingArray(ArrayStack_s* pArrayStack, uint8_t fDouble)
{
    if (pArrayStack != NULL)
    {
        size_t newSize = 0;
        
        //Flag to indicate whether we are shrinking or bloating the backing array
        if (fDouble)
        {
            newSize = pArrayStack->uiSize * 2;
        }
        else
        {
            newSize = pArrayStack->uiSize / 2;
        }
        
        //create new backing array of twice the original size        
        void** newBackingArray = malloc(newSize * sizeof(void*));
        //memset(newBackingArray, 0x00, newSize);
        //copy elemets across
        size_t i = 0;
        for (i = 0; i < pArrayStack->uiCurrentPosition; i++)
        {
            newBackingArray[i] = pArrayStack->backingArray[i];
            pArrayStack->backingArray[i] = NULL;        
        }
        //free the previous backing array
        free(*pArrayStack->backingArray);
        free(pArrayStack->backingArray);
        //update ArrayStack struct
        pArrayStack->backingArray = newBackingArray;
        pArrayStack->uiSize = newSize;
    }
    else
    {
        printf("ArrayStack is NULL\n");
    }
    
}

void* getValue(ArrayStack_s* pArrayStack, size_t uiPosition)
{
    if ((pArrayStack != NULL) && (uiPosition < pArrayStack->uiSize))
    {
        return pArrayStack->backingArray[uiPosition];
    }
    else
    {
        printf("ArrayStack is NULL or givent position, %zu is incorrect \n", uiPosition);
        return NULL;
    }
}

void* removeValue(ArrayStack_s* pArrayStack, size_t uiPosition)
{
    void* pReturnData = NULL;
    if ((pArrayStack != NULL) && (uiPosition < pArrayStack->uiSize))
    {
        //get value, then move all entries up by one position
        pReturnData = pArrayStack->backingArray[uiPosition];
        size_t i = 0;
        for (i = uiPosition; i < pArrayStack->uiCurrentPosition - 1; i++)
        {
            pArrayStack->backingArray[i] = pArrayStack->backingArray[i + 1];
        }
        //update tail end counter
        pArrayStack->uiCurrentPosition = pArrayStack->uiCurrentPosition - 1;
        //check whether we can shrink the backing array
        if ((pArrayStack->uiCurrentPosition > 10) && (pArrayStack->uiCurrentPosition < pArrayStack->uiSize/4))
        {
            resizeBackingArray(pArrayStack, 0);
        }
    }
    else
    {
        printf("ArrayStack is NULL or givent position, %zu is incorrect \n", uiPosition);        
    }
    return pReturnData;
}

void* popStack(ArrayStack_s* pArrayStack)
{
    void* pData = NULL;
    if (pArrayStack != NULL)
    {
        pData = removeValue(pArrayStack, 0);
    }
    else
    {
        printf("ArrayStack is NULL\n");
    }
    
    return pData;
}

void queueValue(ArrayStack_s* pArrayStack, void* pData)
{
    if (pArrayStack != NULL)
    {
        if ((pArrayStack->uiCurrentPosition + 1) < pArrayStack->uiSize)
        {
            //space available so add            
            pArrayStack->backingArray[pArrayStack->uiCurrentPosition] = pData;
            pArrayStack->uiCurrentPosition = pArrayStack->uiCurrentPosition + 1;            
        }
        else
        {
            //no space, so resize then add
            resizeBackingArray(pArrayStack, 1);
            queueValue(pArrayStack, pData);
        }
    }
    else
    {
        printf("ArrayStack is NULL\n");
    }
}

void pushStack(ArrayStack_s* pArrayStack, void* pData)
{
    if (pArrayStack != NULL)
    {        
        if ((pArrayStack->uiCurrentPosition + 1) < pArrayStack->uiSize)
        {
            //space available so add
            //copy all values on position up, starting from bottom of stack            
            if (pArrayStack->uiCurrentPosition != 0)
            {                
                //more than 1 entry in stack
                int32_t i = 0;
                for (i = pArrayStack->uiCurrentPosition; i > 0 ; i--)
                {
                    pArrayStack->backingArray[i] = pArrayStack->backingArray[i - 1];
                }    
            }
                        
            pArrayStack->backingArray[0] = pData;
            pArrayStack->uiCurrentPosition = pArrayStack->uiCurrentPosition + 1;
        }
        else
        {
            //no space, so resize then add
            resizeBackingArray(pArrayStack, 1);
            pushStack(pArrayStack, pData);
        }
    }
    else
    {
        printf("ArrayStack is NULL\n");
    }
}

void destroy(ArrayStack_s* pArrayStack, void (*freeElementData)(void* pData))
{
    if (pArrayStack != NULL)
    {
        if (freeElementData != NULL)
        {
            size_t i = 0;
            for (i = 0; i < pArrayStack->uiCurrentPosition; i++)
            {               
                freeElementData(pArrayStack->backingArray[i]);
                pArrayStack->backingArray[i] = NULL;
            }
        }
        //free(*pArrayStack->backingArray);
        free(pArrayStack->backingArray);
        free(pArrayStack);        
    }
    else
    {
        printf("ArrayStack is NULL\n");
    }
}

size_t getArrayStackSize(ArrayStack_s* pArrayStack)
{
    return pArrayStack->uiCurrentPosition;
}