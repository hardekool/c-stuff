#ifndef ARRAYSTACK_H_INCLUDED
#define ARRAYSTACK_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct ArrayStack_s ArrayStack_s;

    ArrayStack_s* newArrayStack(size_t initialSize);
    void setValue(ArrayStack_s* pArrayStack, void* pData, size_t uiPosition);
    void queueValue(ArrayStack_s* pArrayStack, void* pData); //added at end of arrayStack FIFO style
    void* getValue(ArrayStack_s* pArrayStack, size_t uiPosition);
    size_t getArrayStackSize(ArrayStack_s* pArrayStack);
    void* removeValue(ArrayStack_s* pArrayStack, size_t uiPosition); //remove from specified position and moves all entries up
    void* popStack(ArrayStack_s* pArrayStack);
    void pushStack(ArrayStack_s* pArrayStack, void* pData);
    void destroy(ArrayStack_s* pArrayStack, void (*freeElements)(void* pData));

#ifdef __cplusplus
}
#endif

#endif // ARRAYSTACK_H_INCLUDED