Implementation of array backed queue.
Initial size of backing array specified on creation of arrayStack.
The backing array will grow as more entries are added.

Usage:
cd arrayStack/src
make clean
make

This will produce a arrayStack.so in arrayStack/lib
