Repo containing implementation of Linked list data structures. A test program is included for conveniance.

Usage:
	cd testLists
	make cleanall && make
	cd testLists/bin
	./ListTest

This will make both SLList.so and DLList.so and link with ListTest binary.
