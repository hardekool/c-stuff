#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SLList.h"

typedef struct Node_s
{
    void* pData;
    struct Node_s* pNodeNext;
} Node_s;

typedef struct List_s
{
    Node_s* pNodeHead;
    Node_s* pNodeTail;
    Node_s* pCurrent;
    uint32_t numElements;
} List_s;

/* Allocate memory for list and initialise list values*/
List_s* newSLList()
{
    List_s* pList = (List_s*) malloc(sizeof(List_s));

    if (pList == NULL)
    {
        printf("ERROR creating new SLList\n");
        return NULL;
    }

    initializeList(pList);

    return pList;
}

/* Allocate memory for Node and sets data value */
Node_s* newNode(void* pData)
{
    Node_s* pNode = (Node_s*) malloc(sizeof(Node_s));

    pNode->pData = pData;
    pNode->pNodeNext = NULL;

    return pNode;
}

/* Initialize list */
void initializeList(List_s* pList)
{
    pList->pNodeHead = NULL;
    pList->pNodeTail = NULL;
    pList->pCurrent = NULL;
    pList->numElements = 0;
}

/* Add Node to head of list */
void addHead(List_s* pList, void* pData)
{
    Node_s* pNode = newNode(pData);

    //first node in list
    if (pList->pNodeHead == NULL)
    {
        pList->pNodeTail = pNode;
    }
    else
    {
        pNode->pNodeNext = pList->pNodeHead;
    }
    pList->pNodeHead = pNode;
    pList->pCurrent = pList->pNodeHead;
    pList->numElements++;
}

/* Add node to tail of list */
void addTail(List_s* pList, void* pData)
{
    Node_s* pNode = newNode(pData);

    //no nodes in List
    if (pList->pNodeHead == NULL)
    {
        pList->pNodeHead = pNode;
    }
    else
    {
        pList->pNodeTail->pNodeNext = pNode;
    }
    pList->pNodeTail = pNode;
    pList->numElements++;
}

uint32_t getListSize(List_s* pList)
{
    if (pList != NULL)
        return pList->numElements;

    return 0;
}

/* Remove specified node in list and free's node */
void deleteNode(List_s* pList, Node_s* pNode, void (*freeNodeData)(void* pData))
{
    //No node to free
    if (pNode == NULL)
    {
        printf("ERROR, no node to delete\n");
    }
    else
    {
        //node to delete is at head
        if (pList->pNodeHead == pNode)
        {
            //only node in list
            if (pNode->pNodeNext == NULL)
            {
                pList->pNodeHead = NULL;
                pList->pNodeTail = NULL;

                pNode->pNodeNext = NULL;
            }
            else
            {
                pList->pNodeHead = pNode->pNodeNext;

                pNode->pNodeNext = NULL;
            }
        }
        else
        {
            //traverse list from head to before node to be deleted
            Node_s* pTempNode = pList->pNodeHead;
            while (pTempNode != NULL && pTempNode->pNodeNext != pNode)
            {
                pTempNode = pTempNode->pNodeNext;
            }

            pTempNode->pNodeNext = pNode->pNodeNext;

            pNode->pNodeNext = NULL;
        }
        freeNodeData(pNode->pData);
        pNode->pData = NULL;
        free(pNode);
        pNode = NULL;
        pList->numElements--;
    }
}

void findNode(Node_s* pNode, const void* pData, int8_t (*pfCompareCallback)(const void*, const void*), uint8_t* fFound)
{
    if (pNode == NULL)
        *fFound = 0;
    else
    {
        if (pfCompareCallback(pData, pNode->pData) == 0) //node found exit
            *fFound = 1;
        else findNode(pNode->pNodeNext, pData, pfCompareCallback, fFound); //recursively search through list
    }
}

//returns true if node is found in list
uint8_t findData(List_s* pList, const void* pData, int8_t (*pfCompareCallback)(const void*, const void*))
{
    uint8_t fFound = 0;
    if ((pList == NULL) || (pData == NULL))
        fFound = 0;
    else
    {
        findNode(pList->pNodeHead, pData, pfCompareCallback, &fFound);
    }

    return fFound;

}

/* Display entire list */
void printList(List_s* pList, void (*printData)(const void*))
{
    printf("Linked List\n");

    if (pList == NULL || pList->pNodeHead == NULL)
    {
        printf("ERROR no list or items to display\n");
    }
    else
    {
        Node_s* pNode = pList->pNodeHead;
        while (pNode != NULL)
        {
            printData(pNode->pData);
            pNode = pNode->pNodeNext;
        }
    }
}

/* deletes all nodes in list and free's memory */
void deleteList(List_s* pList, void (*freeNodeData)(void* pData))
{
    if (pList == NULL)
    {
        printf("No list to delete\n");
    }
    else
    {
        while (pList->pNodeHead != NULL)
        {
            deleteNode(pList, pList->pNodeHead, freeNodeData);
        }
        free(pList);
        pList = NULL;
    }
}

void gotoNext(List_s* pList) 
{
    if (pList != NULL && hasNext(pList))
    {
        pList->pCurrent = pList->pCurrent->pNodeNext;
    }
}

/*
List_s* sortList(List_s* pList, int8_t(*pfCompareCallback)(const void*, const void*))
{
    Node_s* tempNode = pList->pNodeHead;
    while (tempNode->pNodeNext != NULL)
    {
        if (pfCompareCallback(tempNode->pData, tempNode->pNodeNext->pData) < 0) //node A smaller than node B data
        {
            Node_s* pPlaceholder = tempNode;
            tempNode->pNodeNext = tempNode->pNodeNext->pNodeNext;
            pPlaceholder->pNodeNext = tempNode;

        }
        else
        {
            tempNode = tempNode->pNodeNext;
        }
    }
}*/

uint8_t hasNext(List_s* pList)
{
    if (pList != NULL)
    {
        
        if (pList->pCurrent != NULL && pList->pCurrent->pNodeNext != NULL)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

void resetCurrentToHead(List_s* pList)
{
    if (pList != NULL)
    {
        pList->pCurrent = pList->pNodeHead;
    }
}

size_t getArray(List_s* pList, void** buffer)
{
    
    size_t counter = 0;
    if (pList != NULL)
    {
        void** returnArray = malloc(pList->numElements * sizeof(void*));
        
        if (pList->pCurrent != NULL)
        {
            returnArray[counter] = pList->pCurrent->pData;
            counter++;        
        }
        
        while(hasNext(pList))
        {
            gotoNext(pList);
            returnArray[counter] = pList->pCurrent->pData;            
            counter++;
        }            
                    
        *buffer = returnArray;        
    }
    return counter;
}

void* getNextData(List_s* pList)
{
    if (pList != NULL && pList->pCurrent != NULL && pList->pCurrent->pNodeNext != NULL)
    {
        pList->pCurrent = pList->pCurrent->pNodeNext;
        return pList->pCurrent->pData;
    }
    else
    {
        return NULL;
    }
}

void* getCurrentData(List_s* pList)
{
    if (pList != NULL && pList->pCurrent != NULL)
    {
        return pList->pCurrent->pData;
    }
    else
    {
        return NULL;
    }
}

void* getHeadData(List_s* pList)
{
    if (pList != NULL)
        return pList->pNodeHead->pData;

    return NULL;
}

void* getTailData(List_s* pList)
{
    if (pList != NULL)
        return pList->pNodeTail->pData;

    return NULL;
}

int8_t compareElements(const void* pTemplate, const void* pData, int8_t(*pfCompareCallback)(const void*, const void*))
{
    //expected <0 for smaller, >0 for larger and ==0 for equal
    return (pfCompareCallback(pTemplate, pData));
}
