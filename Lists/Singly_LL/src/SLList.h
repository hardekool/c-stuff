#ifndef SLLIST_H_INCLUDED
#define SLLIST_H_INCLUDED

#include <stdint.h> //for uint8_t etc.

#ifdef __cplusplus
extern "C"
{
#endif
typedef struct List_s List_s;
typedef struct Node_s Node_s;

List_s* newSLList();
void initializeList(List_s* pList);
void addHead(List_s* pList, void* pData);
void addTail(List_s* pList, void* pData);
void deleteNode(List_s* pList, Node_s* pNode, void (*freeNodeData)(void* pData)); //callback function required to free user defined data
void printList(List_s* pList, void (*printData)(const void*)); //callback function required to display data in list
void deleteList(List_s* pList, void (*freeNodeData)(void* pData));
uint8_t findData(List_s* pList, const void* pData, int8_t (*compareData)(const void*, const void*)); //callback function to compare user defined data
List_s* sortList(List_s* pList, int8_t (*compareData)(const void*, const void*)); //sorts list from larger at head to smaller according to callback function
uint32_t getListSize(List_s* pList);
void* getHeadData(List_s* pList);
void* getTailData(List_s* pList);
uint8_t hasNext(List_s* pList);
void* getNextData(List_s* pList);
void* getCurrentData(List_s* pList);
void gotoNext(List_s* pList);
size_t getArray(List_s* pList, void** buffer);
void resetCurrentToHead(List_s* pList);

#ifdef __cplusplus
}
#endif

#endif // SLLIST_H_INCLUDED
