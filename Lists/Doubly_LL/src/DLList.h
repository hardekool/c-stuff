#ifndef DLLIST_H_INCLUDED
#define DLLIST_H_INCLUDED
typedef struct Node_s Node_s;
typedef struct List_s List_s;

List_s* newDLList();
void initializeList(List_s* pList);
void addHead(List_s* pList, void* pData);
void addTail(List_s* pList, void* pData);
void deleteNode(List_s* pList, Node_s* pNode, void (*freeNodeData)(void* pData)); //callback function required to free user defined data
void printList(List_s* pList, void (*printData)(const void*)); //callback function required to display data in list
void deleteList(List_s* pList, void (*freeNodeData)(void* pData));

#endif // DLLIST_H_INCLUDED
