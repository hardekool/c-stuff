#include <stdio.h>
#include <stdlib.h>
#include "DLList.h"

typedef struct Node_s
{
    void* pData;
    struct Node_s* pNodeNext;
    struct Node_s* pNodePrevious;
} Node_s;

typedef struct List_s
{
    Node_s* pNodeHead;
    Node_s* pNodeTail;
} List_s;

/* Allocate memory for list and initialise list values*/
List_s* newDLList()
{
    List_s* pList = (List_s*) malloc(sizeof(List_s));

    if (pList == NULL)
    {
        printf("ERROR creating new SLList\n");
        return NULL;
    }

    initializeList(pList);

    return pList;
}

/* Allocate memory for Node and sets data value */
Node_s* newNode(void* pData)
{
    Node_s* pNode = (Node_s*) malloc(sizeof(Node_s));

    pNode->pData = pData;
    pNode->pNodeNext = NULL;
    pNode->pNodePrevious = NULL;

    return pNode;
}

/* Initialize list */
void initializeList(List_s* pList)
{
    pList->pNodeHead = NULL;
    pList->pNodeTail = NULL;
}

/* Add Node to head of list */
void addHead(List_s* pList, void* pData)
{
    Node_s* pNode = newNode(pData);

    //first node in list
    if (pList->pNodeHead == NULL)
    {
        pList->pNodeTail = pNode;
    }
    else
    {
        pNode->pNodeNext = pList->pNodeHead;
    }
    pList->pNodeHead = pNode;
}

/* Add node to tail of list */
void addTail(List_s* pList, void* pData)
{
    Node_s* pNode = newNode(pData);

    //no nodes in List
    if (pList->pNodeHead == NULL)
    {
        pList->pNodeHead = pNode;
    }
    else
    {
        pList->pNodeTail->pNodeNext = pNode;
    }
    pNode->pNodePrevious = pList->pNodeTail;
    pList->pNodeTail = pNode;
}

/* Remove specified node in list and free's node */
void deleteNode(List_s* pList, Node_s* pNode, void (*freeNodeData)())
{
    //No node to free
    if (pNode == NULL)
    {
        printf("ERROR, no node to delete\n");
    }
    else
    {
        //node to delete is at head
        if (pList->pNodeHead == pNode)
        {
            //only node in list
            if (pNode->pNodeNext == NULL)
            {
                pList->pNodeHead = NULL;
                pList->pNodeTail = NULL;

                pNode->pNodeNext = NULL;
            }
            else
            {
                pList->pNodeHead = pNode->pNodeNext;
                //pNode->pNodePrevious already NULL because this node is head
                pNode->pNodeNext = NULL;
            }
        }
        else
        {
            //Node is not located at head
            Node_s* pPrevNode = pNode->pNodePrevious;
            Node_s* pNextNode = pNode->pNodeNext;

            //unlink node from doubly linked list
            pPrevNode->pNodeNext = pNextNode;
            if (pNextNode == NULL) //NODE is tail
                pList->pNodeTail = pPrevNode;
            else
                pNextNode->pNodePrevious = pPrevNode;

            pNode->pNodeNext = NULL;
            pNode->pNodePrevious = NULL;
        }
        //free data associated with node
        freeNodeData(pNode->pData);
        pNode->pData = NULL;
        free(pNode);
        pNode = NULL;
    }
}

/* Display entire list */
void printList(List_s* pList, void (*printData)(const void*))
{
    printf("Linked List\n");

    if (pList == NULL || pList->pNodeHead == NULL)
    {
        printf("ERROR no list or items to display\n");
    }
    else
    {
        Node_s* pNode = pList->pNodeHead;
        while (pNode != NULL)
        {
            printData(pNode->pData);
            pNode = pNode->pNodeNext;
        }
    }
}

/* deletes all nodes in list and free's memory */
void deleteList(List_s* pList, void (*freeNodeData)(void* pData))
{
    if (pList == NULL)
    {
        printf("No list to delete\n");
    }
    else
    {
        while (pList->pNodeHead != NULL)
        {
            deleteNode(pList, pList->pNodeHead, freeNodeData);
        }
        free(pList);
        pList = NULL;
    }
}
