#include <stdio.h>
#include <stdlib.h>
#include "../../Singly_LL/src/SLList.h"
#include "../../Doubly_LL/src/DLList.h"

//Data struct
typedef struct Data_s
{
    int mass;
    char* name;
    int* grades;
} Data_s;

void printData(const void* pData)
{
    //cast void* to implimenation specific type
    Data_s* pThisData = (Data_s*) pData;

    printf("%s: %d\n", pThisData->name, pThisData->mass);
    int i = 0;

    for (i = 0; i < 10; i++)
    {
        printf("grades[%d]: %d\n", i, pThisData->grades[i]);
    }
}

void deleteData(void* pData)
{
    //cast void* to implimenation specific type
    Data_s* pThisData = (Data_s*) pData;

    //free malloced memory
    free(pThisData->grades);

}

int8_t compareData(const void* pDataTemplate, const void* pData)
{
    Data_s* pStructTemplateData = (Data_s*) pDataTemplate;
    Data_s* pStructCompareData = (Data_s*) pData;

    if (pStructTemplateData->mass > pStructCompareData->mass)
    {
        return 1;
    }
    else if (pStructTemplateData->mass < pStructCompareData->mass)
    {
        return -1;
    }
    return 0;
}

int main()
{
    List_s* pList = newSLList();

    int* grades1 = (int*) malloc(10 * sizeof(int));
    int* grades2 = (int*) malloc(10 * sizeof(int));
    int* grades3 = (int*) malloc(10 * sizeof(int));
    int* grades4 = (int*) malloc(10 * sizeof(int));
    int* grades5 = (int*) malloc(10 * sizeof(int));

    int i = 0;
    for (i = 0; i < 10; i++)
    {
        grades1[i] = i;
        grades2[i] = -1 * i;
        grades3[i] = i * 2;
        grades4[i] = i * -2;
        grades5[i] = i + 1;
    }

    Data_s DataStruct1 = {.name = "wally", .mass = 50, .grades = grades1};
    Data_s DataStruct2 = {.name = "son", .mass = 54689, .grades = grades2};
    Data_s DataStruct3 = {.name = "pbody", .mass = 125, .grades = grades3};
    Data_s DataStruct4 = {.name = "wally", .mass = 50, .grades = grades4};
    Data_s DataStruct5 = {.name = "john", .mass = 45, .grades = grades5};

    addHead(pList, &DataStruct1);
    addTail(pList, &DataStruct2);
   // addTail(pList, &DataStruct3);
    addHead(pList, &DataStruct4);
    addTail(pList, &DataStruct5);

    printList(pList, printData);

    if (findData(pList, &DataStruct3, compareData))
    {
        printf("Find data !!!!!!!!!!\n");
        printData((const void*) &DataStruct3);
    }
    else
        printf("Node not found\n");

    printf("Size of list: %d\n", getListSize(pList));

    Data_s* pDataStruct = (Data_s*) getHeadData(pList);
    printData(pDataStruct);

    pDataStruct = (Data_s*) getTailData(pList);
    printData(pDataStruct);

    deleteList(pList, deleteData);
    return 0;
}
